/*
 * eventManager.js
 *
 * Contains methods dealing with event listeners
 * and event handling.
 *
 * @author John Olennikov, Devin Helmgren
 */

define(['jquery', 'dataManager', 'viewManager'], function($, dm, vm)
{
    var map;

    /*
     *  init()
     *  
     *  This function creates a Google map object centered 
     *  on the US.
     *  Using jQuery, it also sets up all listeners and create
     *  event handler methods that respond to user events
     *
     */
    var init = function() 
    {

        // The center point of the map is Lincoln, NB.
        var usa = new google.maps.LatLng(40.8358, -96.3552);

        // Create a Google Map object centered at usa
        map = new google.maps.Map(document.getElementById("contig-us-map-canvas"), 
        {
            zoom : 4,
            center : usa,
            mapTypeId : google.maps.MapTypeId.ROADMAP
        });

        // Add the "map" variable to dataManager file
        dm.setMap(map);

        // Toggles the Data Table radio buttons
        $("#number-button, #location-button").on("click", function() 
        {
            // Caches the current object as a jQuery selector
            var $this = $(this);

            // Toggles the "active-button" class on the clicked label
            $this.siblings("label").removeClass("active-button");
            $this.next("label.button").addClass("active-button");

            onSortClick();
        });

        // Add a listener for counseleor selection drop-down menu
        $("#drop-down").on("change", function() 
        {
            var $this=$(this);

            // Remove the "Select Counselor" after a selection
            // is make to ensure a blank is never selected
            var $blank = $this.find("option[value='']");
            if ($blank.length > 0) 
            {
                $blank.remove();
            }

            onCounselorSelect();
        });
        
        // Add a change listener for application type radio button change
        $("#radio-buttons input").on("change", function() 
        {
            onAppStatusClick();
        });

        // Add fusion table points click listener
        google.maps.event.addListener(dm.getFusionTable(), 'click', function(event) 
        {
            onSelection(event.row.HSCode.value, new Array(event.latLng.lat(),event.latLng.lng()));
        }); 

        // Add data table click listener
        $( "#right-info-data" ).on("click",".data-box-contain", function(event) 
        {
            var $this = $(this);
            onSelection($this.attr("id").split("-")[2], $this.data("latlng").split("#"));
        });

    };

    /*
     *
     * The following function are functions that respond 
     * the the listener events on the page
     *
     */

    /*
     * onSortClick()
     * 
     * This function responds to users request to sort the school 
     * listings in the right-hand data list.  The user makes this 
     * request by selecting either "Number" or "Location" radio button.
     *
     */ 
    var onSortClick = function()
    {
        dm.dataSort();
        vm.updateView(map,dm.getSchoolData());
    }

    /*
     * onSelection()
     * 
     * This function responds to users request to select schools based
     * on the map makers or highlighted table entries.
     *
     *@param code The high school CEEB code
     *@param latLng The map location of the high school
     */ 
    var onSelection = function(code,latLng)
    {
        vm.updateSelection(map,code,latLng);
    }


    /*
     * onCounselorSelect()
     * 
     * This function responds to the counselor change listener
     * and updates map with appropriate counselor information.
     *
     */ 
    var onCounselorSelect = function() 
    {
        dm.setSelectedCounselor(vm.getCounselor());

        dm.queryData();
    };

    /*
     * onAppStatusClick()
     * 
     * This function responds to the application status listener
     * and updates map with appropriate app status information.
     *
     */ 
    var onAppStatusClick = function() 
    {
        if (vm.getCounselor() == "") 
        {
            alert("Please select a counselor.");
            return false;
        }
        dm.setSelectedAppStatus(vm.getAppStatus());
    
        dm.queryData();
    };

    // Any functions defined in this return statement are considered public
    // functions by RequireJS, and accessible through the namespace that
    // is attached to this module when loaded in other files.
    return {
        init : init,
        onSortClick : onSortClick,
        onSelection : onSelection,
        onCounselorSelect : onCounselorSelect,
        onAppStatusClick : onAppStatusClick,
   };
});
